sap.ui.define([
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/Input",
	"sap/m/Button",
	"sap/m/TextArea",
	"sap/ui/layout/HorizontalLayout",
	"sap/ui/layout/VerticalLayout",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller",
	"swe/model/models",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Dialog, Text, Input, Button, TextArea, HorizontalLayout, VerticalLayout, JSONModel, Controller, models, ODataModel, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("swe.controller.material", {
		 
		onInit: function(evt) {
			
			var load = evt.mParameters.id; //eslint-disable-line

	
	
		},
		
		onAfterRendering: function(){
			var a = this.byId("lblPageTitle").getText();
			var b = this.byId("material").getText();
			this.byId("lblPageTitle").setText(a + "-" + b);
			
		},

		getModel: function(sName) {
			return this.getView().getModel(sName);
		},
		
		onCollapseAll: function() {
			var oTreeTable = this.getView().byId("TreeTableBasic");
			oTreeTable.collapseAll();
		},

		onExpandFirstLevel: function() {
			var oTreeTable = this.getView().byId("TreeTableBasic");
			oTreeTable.expandToLevel(1);
		},
		onMenuPress: function(oEvent) {
			var oButton = oEvent.getSource();

			if (!this._menu) {
				this._menu = sap.ui.xmlfragment(
					"swe.view.fragment.menu",
					this
				);
				this.getView().addDependent(this._menu);

			}

			var eDock = sap.ui.core.Popup.Dock;
			this._menu.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);

		},
		ItemPressed: function(oEvent, position) {
			var msg = oEvent.getParameter("item").getText();

			if (msg === "Über diese App") {
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess); //eslint-disable-line
				this.onAboutPress(oEvent);

			} else if (msg === "About This App") {
				this.onAboutPress(oEvent);
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess);  //eslint-disable-line
			} else if (msg === "Zurück") {

				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("first");
			}
		},
		
			/**
		 * Event handler when the about app menu item is pressed. Re-uses the help dialog.
		 * @public
		 */
		onAboutPress: function(oEvent) {
		    var oDialog = sap.ui.xmlfragment("swe.view.fragment.AboutFragment", this);
			oDialog.setModel(this.getModel("i18n"), "i18n");
			oDialog.setModel(this.getOwnerComponent().getModel("local"), "local");
			
			//Dialog custom header
		//	sap.ui.getCore().byId("dlgAboutTitle").setText(this.getText("app.AboutDialogTitle"));
			
			var oLayout = sap.ui.getCore().byId("gridAbout"),
				oFragment = sap.ui.xmlfragment("swe.view.fragment.aboutTheApp", this);
			oLayout.addContent(oFragment);
			oDialog.open();
		},
		
			/**
		 * Closes the about dialog
		 * @public
		 */
		onDialogCloseAbout: function() {
			this._closeDialog("dlgAbout");
		},
		
		onGeoSuccess: function(position){
			sap.ui.getCore().byId("sLat").setText(position.coords.latitude);
			sap.ui.getCore().byId("sLong").setText(position.coords.longitude);
		},

		onBarCodeScan: function() {
			var that = this;
			var code = "";
			cordova.plugins.barcodeScanner.scan(
				function(result) {
					code = result.text;
					console.log("This is the code: " + code); //eslint-disable-line
					sap.ui.getCore().byId("inScan").setValue(code);
					that.onSearch();
				},
				function(error) {
					alert("Scanning failed: " + error); //eslint-disable-line
				}
			);
		},
		
		scn: function(){
			var scanNumber = sap.ui.getCore().byId("inScan").setValue();	
		},
		
		onNavBack: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("first");	
		},
		
		onAddNewMat: function(oEvent, fragment, titlePart){
			var oMatDialog = sap.ui.xmlfragment("swe.view.fragment.newMatDialog", this);
				oMatDialog.setModel(this.getModel("i18n"), "i18n");
				oMatDialog.open();
		},
		
		onDialogCloseMat:function(oEvent){
		this._closeDialog("dlgMat");
			
		},
		
		onDialogCloseaboutTheApp: function() {
				this._closeDialog("dlgaboutTheApp");
		},
		
		_closeDialog: function(dialogId) {
			var oDialog = sap.ui.getCore().byId(dialogId);
				oDialog.close();
				oDialog.destroy();
		},
		
		onInfoPress: function(oEvent, fragment, titlePart) {
			var oHelpDialog = sap.ui.xmlfragment("swe.view.fragment.HelpDialog", this);
				oHelpDialog.setModel(this.getModel("i18n"), "i18n");        // Verknüpfung zu i18n 
			//	sap.ui.getCore().byId("dlgHelpTitle");  //This uses dialog custom header
				oHelpDialog.open();
		},
		onDialogCloseHelp: function() {
		    this._closeDialog("dlgHelp");
		},
		
		Save_Mat: function(){
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				var Material = sap.ui.getCore().byId("inScan").getValue();
				
				switch (Material){
					case "1234":
						var oMat = {
							MAT_ID: "8",
							Kategorie: "Hammer",
							STK: "20",
							Bezeichnung: "Holzhammer"
						};	
						this._onSavePush(oMat);
						break;
					case "4567":
						var oMat = {
							MAT_ID: "8",
							Kategorie: "Reinigunstücher",
							STK: "112",
							Bezeichnung: "Taschentücher"
						};
						this._onSavePush(oMat);
						break;
					case "7890":
						var oMat = {
							MAT_ID: "8",
							Kategorie: "Dixie",
							STK: "350",
							Bezeichnung: "Plumpsklo"
						};
						this._onSavePush(oMat);
						break;
				}
				this._closeDialog("dlgMat");
				oRouter.navTo("material");
			
		},
		
		_onSavePush: function(oMat){
			var aData = this.getOwnerComponent().getModel("local").getProperty("/material");
			aData.push(oMat);
			this._onRefreshModel();
		},
		
		_onRefreshModel: function(){
			var oModel = this.getOwnerComponent().getModel("local");
			oModel.refresh();
		},
		
		onMatFilterPress: function(oEvent) {
			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("Kategorie", FilterOperator.Contains, sQuery));
			}
			
			// filter binding
			var oList = this.getView().byId("MaraTable");
			var oBinding = oList.getBinding("items");
				oBinding.filter(aFilter);
		
		}
	});
});