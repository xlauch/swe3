sap.ui.define([
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/Button",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Dialog, Text, Button, Controller, ODataModel, JSONModel, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("swe.controller.firmenwagen", {
		onInit: function(evt, require) {

			/*var oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);*/

			//	oModel.getProperty("/test")

			var load = evt.mParameters.id; //eslint-disable-line
			
			var oLocalModel = this.getOwnerComponent().getModel("localmodel");
			oLocalModel.attachRequestCompleted(this.test(oLocalModel));
			
			oLocalModel.getProperty("/test");
			if (load === "__component0---first") {
				// set mock model
				var sPath = jQuery.sap.getModulePath("swe.model", "/localmodel.json");
				var oModel = new JSONModel(sPath);
				this.getView().setModel(oModel);

			} else if (load === "__component0---firmenwagen") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			}
			this._initModel();
		},
		
		onAfterRendering: function(){
			var a = this.byId("lblPageTitle").getText();
			var b = this.byId("wagen").getText();
			this.byId("lblPageTitle").setText(a + "-" + b);
			
		},
		
		test: function(Model){
			var test = Model;
		},
		

		_initModel: function() {
			var oModel = this.getOwnerComponent().getModel("catalog").getProperty("/Offene");
//			this.fillData();
		},
		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		onMenuPress: function(oEvent) {
			var oButton = oEvent.getSource();

			if (!this._menu) {
				this._menu = sap.ui.xmlfragment(
					"swe.view.fragment.menu",
					this
				);
				this.getView().addDependent(this._menu);

			}

			var eDock = sap.ui.core.Popup.Dock;
			this._menu.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);

		},
			ItemPressed: function(oEvent, position) {
			var msg = oEvent.getParameter("item").getText();

			if (msg === "Über diese App") {
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess); //eslint-disable-line
				this.onAboutPress(oEvent);

			} else if (msg === "About This App") {
				this.onAboutPress(oEvent);
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess);  //eslint-disable-line
			} else if (msg === "Zurück") {

				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("first");
			}
		},
		
			/**
		 * Event handler when the about app menu item is pressed. Re-uses the help dialog.
		 * @public
		 */
		onAboutPress: function(oEvent) {
		    var oDialog = sap.ui.xmlfragment("swe.view.fragment.AboutFragment", this);
			oDialog.setModel(this.getModel("i18n"), "i18n");
			oDialog.setModel(this.getOwnerComponent().getModel("local"), "local");
			
			//Dialog custom header
		//	sap.ui.getCore().byId("dlgAboutTitle").setText(this.getText("app.AboutDialogTitle"));
			
			var oLayout = sap.ui.getCore().byId("gridAbout"),
				oFragment = sap.ui.xmlfragment("swe.view.fragment.aboutTheApp", this);
			oLayout.addContent(oFragment);
			oDialog.open();
		},
		
			/**
		 * Closes the about dialog
		 * @public
		 */
		onDialogCloseAbout: function() {
			this._closeDialog("dlgAbout");
		},
		onGeoSuccess: function(position){
			sap.ui.getCore().byId("sLat").setText(position.coords.latitude);
			sap.ui.getCore().byId("sLong").setText(position.coords.longitude);
		},
		
		onNavBack: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("first");
		},

		onInfoPress: function(oEvent, fragment, titlePart) {
			var oHelpDialog = sap.ui.xmlfragment("swe.view.fragment.HelpDialog", this);
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			//	sap.ui.getCore().byId("dlgHelpTitle");  //This uses dialog custom header
			oHelpDialog.open();
		},
		
		/**
		 * Closes the aboutTheApp dialog
		 * @public
		 */
			onDialogCloseaboutTheApp: function() {
				this._closeDialog("dlgaboutTheApp");
		},

		/**
		 * Closes the help dialog
		 * @public
		 */
		onDialogCloseHelp: function() {
			this._closeDialog("dlgHelp");
		},
		/**
		 * Closes the Kunde dialog
		 * @public
		 */
		onDialogCloseCar: function() {
			this._closeDialog("dlgCar");
		},

		/**
		 * Generic method to close and destroy dialogs.
		 * @public
		 */
		_closeDialog: function(dialogId) {
			var oDialog = sap.ui.getCore().byId(dialogId);
			oDialog.close();
			oDialog.destroy();
		},

		onAddNewCar: function() {
			var oNewCar = sap.ui.xmlfragment("swe.view.fragment.NewCarDialog", this);
			oNewCar.setModel(this.getModel("i18n"), "i18n");
			oNewCar.open();
			oNewCar.setModel(this.getModel("i18n"), "i18n");
			oNewCar.open();
		},

	Save_Wagen: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oModel = this.getOwnerComponent().getModel("localmodel");

			
			var newWagen = {
				Wagen_ID: sap.ui.getCore().byId("IDCar").getValue(),
				Marke: sap.ui.getCore().byId("MarkeCar").getValue(),
				Modell: sap.ui.getCore().byId("ModelCar").getValue(),
				Hubraum: sap.ui.getCore().byId("HubraumCar").getValue(),
				Kilometerstand: sap.ui.getCore().byId("kmCar").getValue()
			};
				this._onSavePush(newWagen);
				
				this._closeDialog("dlgCar");
				
				oRouter.navTo("firmenwagen");

		},

		_onSavePush: function(newWagen){
			var aData  =	this.getOwnerComponent().getModel("local").getProperty("/wagen");
			aData.push(newWagen);
			this._onRefreshModel();
		},
		
		_onRefreshModel: function(){
			var oModel = this.getOwnerComponent().getModel("local");
			oModel.refresh();
		},


		onWagFilterPress: function(oEvent) {

			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("Marke", FilterOperator.Contains, sQuery));
			}

			// filter binding
			var oList = this.getView().byId("Firmenwagen");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		}

	});

});