sap.ui.define([
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/Button",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Dialog, Text, Button, Controller, ODataModel, JSONModel, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("swe.controller.fertige", {
		onInit: function(evt, require) {

			var load = evt.mParameters.id; //eslint-disable-line
			
			var oLocalModel = this.getOwnerComponent().getModel("localmodel");
			
				
			
			if (load === "__component0---first") {
				// set mock model
				var sPath = jQuery.sap.getModulePath("swe.model", "/localmodel.json");
				var oModel = new JSONModel(sPath);
				this.getView().setModel(oModel);

			} else if (load === "__component0---fertige") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			} 

			
		},
		
		onAfterRendering: function(){
			var a = this.byId("lblPageTitle").getText();
			var b = this.byId("fertige").getText();
			this.byId("lblPageTitle").setText(a + "-" + b);
			
		},
		
		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		onMenuPress: function(oEvent) {
			var oButton = oEvent.getSource();

			if (!this._menu) {
				this._menu = sap.ui.xmlfragment(
					"swe.view.fragment.menu",
					this
				);
				this.getView().addDependent(this._menu);

			}

			var eDock = sap.ui.core.Popup.Dock;
			this._menu.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);

		},
		ItemPressed: function(oEvent, position) {
			var msg = oEvent.getParameter("item").getText();

			if (msg === "Über diese App") {
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess); //eslint-disable-line
				this.onAboutPress(oEvent);

			} else if (msg === "About This App") {
				this.onAboutPress(oEvent);
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess);  //eslint-disable-line
			} else if (msg === "Zurück") {

				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("first");
			}
		},
		
			/**
		 * Event handler when the about app menu item is pressed. Re-uses the help dialog.
		 * @public
		 */
		onAboutPress: function(oEvent) {
		    var oDialog = sap.ui.xmlfragment("swe.view.fragment.AboutFragment", this);
			oDialog.setModel(this.getModel("i18n"), "i18n");
			oDialog.setModel(this.getOwnerComponent().getModel("local"), "local");
			
			//Dialog custom header
		//	sap.ui.getCore().byId("dlgAboutTitle").setText(this.getText("app.AboutDialogTitle"));
			
			var oLayout = sap.ui.getCore().byId("gridAbout"),
				oFragment = sap.ui.xmlfragment("swe.view.fragment.aboutTheApp", this);
			oLayout.addContent(oFragment);
			oDialog.open();
		},
		
			/**
		 * Closes the about dialog
		 * @public
		 */
		onDialogCloseAbout: function() {
			this._closeDialog("dlgAbout");
		},
		
		onGeoSuccess: function(position){
			sap.ui.getCore().byId("sLat").setText(position.coords.latitude);
			sap.ui.getCore().byId("sLong").setText(position.coords.longitude);
		},
		
		onNavBack: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("first");
		},

		onInfoPress: function(oEvent, fragment, titlePart) {
			var oHelpDialog = sap.ui.xmlfragment("swe.view.fragment.HelpDialog", this);
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			//	sap.ui.getCore().byId("dlgHelpTitle");  //This uses dialog custom header
			oHelpDialog.open();
		},
		
		/**
		 * Closes the aboutTheApp dialog
		 * @public
		 */
			onDialogCloseaboutTheApp: function() {
				this._closeDialog("dlgaboutTheApp");
		},

		/**
		 * Closes the help dialog
		 * @public
		 */
		onDialogCloseHelp: function() {
			this._closeDialog("dlgHelp");
		},
		/**
		 * Generic method to close and destroy dialogs.
		 * @public
		 */
		_closeDialog: function(dialogId) {
			var oDialog = sap.ui.getCore().byId(dialogId);
			oDialog.close();
			oDialog.destroy();
		},

		onAuftFilterPress: function(oEvent) {

			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("Ort", FilterOperator.Contains, sQuery));
			}

			// filter binding
			var oList = this.getView().byId("AuftTable");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		},


	});

});