sap.ui.define([
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/Button",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Dialog, Text, Button, Controller, ODataModel, JSONModel, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("swe.controller.neueAuft", {
		onInit: function(evt, require) {

			/*var oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);*/

			//	oModel.getProperty("/test")
				// set explored app's demo model on this sample
			var oImgModel = new JSONModel(jQuery.sap.getModulePath("swe.model", "/images.json"));
			this.getView().setModel(oImgModel, "img");
			
			var load = evt.mParameters.id; //eslint-disable-line
			
			var oLocalModel = this.getOwnerComponent().getModel("localmodel");
			
			oLocalModel.getProperty("/test");
			if (load === "__component0---first") {
				// set mock model
				var sPath = jQuery.sap.getModulePath("swe.model", "/localmodel.json");
				var oModel = new JSONModel(sPath);
				this.getView().setModel(oModel);

			} else if (load === "__component0---neue") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);
			}
		},
		
		onAfterRendering: function(){
			var a = this.byId("lblPageTitle").getText();
			var b = this.byId("auftrag").getText();
			this.byId("lblPageTitle").setText(a + "-" + b);
			
		},

		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},
		
		onMenuPress: function(oEvent) {
			var oButton = oEvent.getSource();

			if (!this._menu) {
				this._menu = sap.ui.xmlfragment(
					"swe.view.fragment.menu",
					this
				);
				this.getView().addDependent(this._menu);

			}

			var eDock = sap.ui.core.Popup.Dock;
			this._menu.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);

		},
		
		ItemPressed: function(oEvent, position) {
			var msg = oEvent.getParameter("item").getText();

			if (msg === "Über diese App") {
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess); //eslint-disable-line
				this.onAboutPress(oEvent);

			} else if (msg === "About This App") {
				this.onAboutPress(oEvent);
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess);  //eslint-disable-line
			} else if (msg === "Zurück") {

				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("first");
			}
		},
		
			/**
		 * Event handler when the about app menu item is pressed. Re-uses the help dialog.
		 * @public
		 */
		onAboutPress: function(oEvent) {
		    var oDialog = sap.ui.xmlfragment("swe.view.fragment.AboutFragment", this);
			oDialog.setModel(this.getModel("i18n"), "i18n");
			oDialog.setModel(this.getOwnerComponent().getModel("local"), "local");
			
			//Dialog custom header
		//	sap.ui.getCore().byId("dlgAboutTitle").setText(this.getText("app.AboutDialogTitle"));
			
			var oLayout = sap.ui.getCore().byId("gridAbout"),
				oFragment = sap.ui.xmlfragment("swe.view.fragment.aboutTheApp", this);
			oLayout.addContent(oFragment);
			oDialog.open();
		},
		
			/**
		 * Closes the about dialog
		 * @public
		 */
		onDialogCloseAbout: function() {
			this._closeDialog("dlgAbout");
		},
		
		onGeoSuccess: function(position){
			sap.ui.getCore().byId("sLat").setText(position.coords.latitude);
			sap.ui.getCore().byId("sLong").setText(position.coords.longitude);
		},
		
		onNavBack: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("first");
		},

		onInfoPress: function(oEvent, fragment, titlePart) {
			var oHelpDialog = sap.ui.xmlfragment("swe.view.fragment.HelpDialog", this);
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			//	sap.ui.getCore().byId("dlgHelpTitle");  //This uses dialog custom header
			oHelpDialog.open();
		},

		onDialogCloseaboutTheApp: function() {
				this._closeDialog("dlgaboutTheApp");
		},
		
		/**
		 * Closes the help dialog
		 * @public
		 */
		onDialogCloseHelp: function() {
			this._closeDialog("dlgHelp");
		},
		
		/**
		 * Generic method to close and destroy dialogs.
		 * @public
		 */
		_closeDialog: function(dialogId) {
			var oDialog = sap.ui.getCore().byId(dialogId);
			oDialog.close();
			oDialog.destroy();
		},
		
		Save_Auf: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oModel = this.getOwnerComponent().getModel("localmodel");
			var StatusOffene = "Offen";
			
			var newAuftr = {
				"Nr": this.getView().byId("IDAuftr").getValue(),
				"Mitarbeiter": this.getView().byId("Mitarbeiter").getValue(),
				"Kunde": this.getView().byId("kunde").getValue(),
				"Ort": this.getView().byId("OrtAuftr").getValue(),
				"Tätigkeit": this.getView().byId("Taetigkeit").getValue(),
				"Bezeichnung": this.getView().byId("Bezeichnung").getValue(),
				"Status": StatusOffene
			};
			
		//	oModel.getProperty("/local/1/model").push(newAuftr);
		//	oModel.refresh();

		//	var oModel = new sap.ui.model.json.JSONModel();
		//	var	oModel = 	this.getOwnerComponent().getModel("catalog").getProperty("/Offene");
			
			var aData  =	this.getOwnerComponent().getModel("local").getProperty("/offene");
				aData.push(newAuftr);
				oRouter.navTo("offene");

		}

	});

});