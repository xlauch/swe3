sap.ui.define([
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/Button",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/odata/v2/ODataModel",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Dialog, Text, Button, Controller, ODataModel, JSONModel, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("swe.controller.first", {
		onInit: function(evt, require) {

			/*var oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);*/

			//	oModel.getProperty("/test")

			var load = evt.mParameters.id; //eslint-disable-line
			
			var oLocalModel = this.getOwnerComponent().getModel("localmodel");
			oLocalModel.attachRequestCompleted(oLocalModel);
			
			oLocalModel.getProperty("/test");
			
			if (load === "__component0---first") {
				// set mock model
				var sPath = jQuery.sap.getModulePath("swe.model", "/localmodel.json");
				var oModel = new JSONModel(sPath);
				this.getView().setModel(oModel);

			} else if (load === "__component0---offene") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			} else if (load === "__component0---fertige") {
					oModel = new sap.ui.model.json.JSONModel();
					oModel.loadData("model/localmodel/local/model");
					this.getView().setModel(oModel);

			} else if (load === "__component0---alle") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			} else if (load === "__component0---neue") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			} else if (load === "__component0---kunden_Tabelle") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			} else if (load === "__component0---personal") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			} else if (load === "__component0---firmenwagen") {
				oModel = new sap.ui.model.json.JSONModel();
				oModel.loadData("model/catalog.json");
				this.getView().setModel(oModel);

			}
			this._initModel();
		},
	
		_initModel: function() {
			var oModel = this.getOwnerComponent().getModel("catalog").getProperty("/Offene");
//			this.fillData();
		},
		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},


		tileNav: function(evt) {

			var tileId = evt.mParameters.id; //eslint-disable-line

			var offene = "__tile0-__component0---first--container-0";
			var fertig = "__tile0-__component0---first--container-1";
			var alle = "__tile0-__component0---first--container-2";
			var neueAuftr = "__tile0-__component0---first--container-3";
			var material = "__tile0-__component0---first--container-4";
			var neuesMat = "__tile0-__component0---first--container-5";
			var personal = "__tile0-__component0---first--container-6";
			var firmenwagen = "__tile0-__component0---first--container-7";

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			if (tileId === offene) {
				oRouter.navTo("offene");
			} else if (tileId === fertig) {
				oRouter.navTo("fertige");
			} else if (tileId === alle) {
				oRouter.navTo("alle");
			} else if (tileId === material) {
				oRouter.navTo("material");
			} else if (tileId === neueAuftr) {
				oRouter.navTo("neueAuftr");
			} else if (tileId === neuesMat) {
				oRouter.navTo("kunden_Tabelle");
			} else if (tileId === personal) {
				oRouter.navTo("personal");
			} else if (tileId === firmenwagen) {
				oRouter.navTo("firmenwagen");
			}
		},
		onMenuPress: function(oEvent) {
			var oButton = oEvent.getSource();

			if (!this._menu) {
				this._menu = sap.ui.xmlfragment(
					"swe.view.fragment.menu",
					this
				);
				this.getView().addDependent(this._menu);

			}

			var eDock = sap.ui.core.Popup.Dock;
			this._menu.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);

		},
		ItemPressed: function(oEvent, position) {
			var msg = oEvent.getParameter("item").getText();

			if (msg === "Über diese App") {
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess); //eslint-disable-line
				this.onAboutPress(oEvent);

			} else if (msg === "About This App") {
				this.onAboutPress(oEvent);
				navigator.geolocation.getCurrentPosition(this.onGeoSuccess);  //eslint-disable-line
			}
		},
		
			/**
		 * Event handler when the about app menu item is pressed. Re-uses the help dialog.
		 * @public
		 */
		onAboutPress: function(oEvent) {
		    var oDialog = sap.ui.xmlfragment("swe.view.fragment.AboutFragment", this);
			oDialog.setModel(this.getModel("i18n"), "i18n");
			oDialog.setModel(this.getOwnerComponent().getModel("local"), "local");
			
			//Dialog custom header
			sap.ui.getCore().byId("dlgAboutTitle").setText(this.getText("app.AboutDialogTitle"));
			
			var oLayout = sap.ui.getCore().byId("gridAbout"),
				oFragment = sap.ui.xmlfragment("swe.view.fragment.aboutTheApp", this);
			oLayout.addContent(oFragment);
			oDialog.open();
		},
		
		onGeoSuccess: function(position){
			sap.ui.getCore().byId("sLat").setText(position.coords.latitude);
			sap.ui.getCore().byId("sLong").setText(position.coords.longitude);
		},

		onInfoPress: function(oEvent, fragment, titlePart) {
			var oHelpDialog = sap.ui.xmlfragment("swe.view.fragment.HelpDialog", this);
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			oHelpDialog.setModel(this.getModel("i18n"), "i18n"); // Verknüpfung zu i18n 
			//	sap.ui.getCore().byId("dlgHelpTitle");  //This uses dialog custom header
			oHelpDialog.open();
		},
		
			/**
		 * Closes the aboutTheApp dialog
		 * @public
		 */
			onDialogCloseaboutTheApp: function() {
				this._closeDialog("dlgaboutTheApp");
		},
		/**
		 * Closes the About dialog
		 * @public
		 */
		onDialogCloseAbout: function() {
			this._closeDialog("dlgAbout");
		},

		/**
		 * Closes the help dialog
		 * @public
		 */
		onDialogCloseHelp: function() {
			this._closeDialog("dlgHelp");
		},
		/**
		 * Generic method to close and destroy dialogs.
		 * @public
		 */
		_closeDialog: function(dialogId) {
			var oDialog = sap.ui.getCore().byId(dialogId);
			oDialog.close();
			oDialog.destroy();
		},

				/**
		 * Handle language click and open language change dialog
		 * @public
		 */
		onChooseLanguage: function() {
			this.baseOnUserContextDialogPress(
				this.getResourceBundle().getText("Sprache auswählen"), //  "userDialog.LanguageTitle"
				"language",
				"language>/Language",
				[],
				"{language>Key}",
				"{language>Name}",
				"onLanguageItemPressed");
		},
			onLanguageItemPressed : function(oController, oEvent) {
			oController.baseOnLanguageChanged(oController, oEvent);
			var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();
			oController.getOwnerComponent().getModel("local").setProperty("/CurrLang", sCurrentLocale);
			
		},
			/**
	     * Opens the passed dialog to choose a user setting, and dynamically assigns the passed click listener
		 * @public
		 */
		baseOnUserContextDialogPress: function(sDialogTitle, sModelName, sPath, aFilters, sItemKey, sItemName, sItemClickListener) {
			var oUserContextDialog = sap.ui.xmlfragment("swe.view.fragment.UserContextDialog", this);
				oUserContextDialog.setModel(this.getModel("i18n"), "i18n");
				oUserContextDialog.setModel(this.getModel(sModelName), sModelName);
			
		    sap.ui.getCore().byId("dlgUserContextDialogTitle").setText(sDialogTitle);
		    
			var oListUserContext = sap.ui.getCore().byId("userContextList");

	        	oUserContextDialog.setModel(this.getView().getModel()); //model is assigned to dialogue
            	oUserContextDialog.setBindingContext(this.getView().getBindingContext());
		    
			var fItemClickListener = this[sItemClickListener];
			var that = this;
		    var oItemTemplate = new sap.m.StandardListItem({
                description: sItemKey,
                title: sItemName,
                type: "Active",
                press: function onItem(parameter) {
				    fItemClickListener(that, parameter);
				    that._closeDialog("dlgUserContext");
			    }
            });
		    
			oListUserContext.bindItems({
				path : sPath,
				filters: aFilters,
				template : oItemTemplate
			});
			oUserContextDialog.open();
		},
		
			/**
		 * Convenience method for getting the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function() {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

			baseOnLanguageChanged : function(oController, oEvent) {
	        oController.onUserContextItemPressed(
	            oController, oEvent, 
	            "local",
	            "/Language",
	            "/LanguageDescription",
	            false);
	       sap.ui.getCore().getConfiguration().setLanguage(oController.getOwnerComponent().getModel("local").getProperty("/Language"));
	       
	       //Re-load page title, which is a generic control and not directly bound to an i18n model
	        var oModelLocal = oController.getOwnerComponent().getModel("local");
				oController.byId("lblPageTitle").setText(oController.getText(oModelLocal.getProperty("/ActivePageTitleI18nKey")));
		},
				/**
	     * Handles user context dialog selection
		 * @public
		 */
	    onUserContextItemPressed : function(that, oEvent, sTargetModel, sSearchModelKey, sUserDescription, bDesciptionWithKey){
		    var oListItem = oEvent.getParameter("listItem") || oEvent.getSource();
		    var title = oListItem.getProperty("title");
		    var key = oListItem.getProperty("description");
		    
		    var oModelSearch = that.getOwnerComponent().getModel(sTargetModel);
				oModelSearch.setProperty(sSearchModelKey, key); 
		    
		    var sDescription = title;
		    if(bDesciptionWithKey){
		        sDescription = key + " - " + sDescription;
		    }
		    oModelSearch.setProperty(sUserDescription, sDescription); 
		},
			/**
		 * Getter for a internationalized text.
		 * @public
		 * @returns {string} the text
		 */
		getText: function(sTextkey) {
			return this.getResourceBundle().getText(sTextkey);
		},
		
	});

});