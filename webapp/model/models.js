sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createLocalModel: function() {
			var oModel = new JSONModel({

				offene: [{
                               "Nr": 1,
                               "Mitarbeiter": "Hans Unterseher",
                               "Kunde": "Kaiser& Kaiser",
                               "Ort": "Bremen",
                               "Tätigkeit": "Unterstützen des IT Aufbaus",
                               "Bezeichnung": "Montage",
                               "Status": "Offen"
                }, {
                               "Nr": 2,
                               "Mitarbeiter": "Anna Kanns",
                               "Kunde": "Ship Shore",
                               "Ort": "Bremerhaven",
                               "Tätigkeit": "Beraten",
                               "Bezeichnung": "Beratung in der It",
                               "Status": "Offen"
                },
                {
                               "Nr": 3,
                               "Mitarbeiter": "Hans Heinz",
                               "Kunde": "Sequdo GmbH",
                               "Ort": "Bremerhaven",
                               "Tätigkeit": "Reparatur",
                               "Bezeichnung": "Instandsetzung der Maschinen",
                               "Status": "Offen"
                },
                               {
                               "Nr": 4,
                               "Mitarbeiter": "Dell Gut",
                               "Kunde": "Hans&Jens GmbH",
                               "Ort": "Bremerhaven",
                               "Tätigkeit": "E-Check",
                               "Bezeichnung": "Elektronik Kontrolle",
                               "Status": "Offen"
                },             {
                               "Nr": 5,
                               "Mitarbeiter": "Sam von Haven",
                               "Kunde": "Güven OHG",
                               "Ort": "Hamburgn",
                               "Tätigkeit": "Ofen Instalation",
                               "Bezeichnung": "Instalation eines Backofens",
                               "Status": "Offen"
                },             {
                               "Nr": 6,
                               "Mitarbeiter": "Anna Kanns",
                               "Kunde": "Hans&Jens GmbH",
                               "Ort": "Bremerhaven",
                               "Tätigkeit": "Beratung",
                               "Bezeichnung": "Beratung neuer IT Lösung im Fachbereich Finanzdienstleistunen",
                               "Status": "Offen"
                },             {
                               "Nr": 7,
                               "Mitarbeiter": "Wolfgang Kühle",
                               "Kunde": "VW Haus Meier",
                               "Ort": "Bremen",
                               "Tätigkeit": "Sicherheits Montage",
                               "Bezeichnung": "Instalation einer neue Alarmanlage",
                               "Status": "Offen"
                },             {
                               "Nr": 8,
                               "Mitarbeiter": "Manfred Intel",
                               "Kunde": "Landschaftsbauer Meier GmbH",
                               "Ort": "Rotenburg Wümme",
                               "Tätigkeit": "Sicherheits Montage",
                               "Bezeichnung": "Instalation einer neue Alarmanlage",
                               "Status": "Offen"
                }],
                material: [{
                               "MAT_ID": 1,
                               "Kategorie": "Hammer",
                               "STK": "25",
                               "Bezeichnung": "Gummihammer"
                },
                {
                               "MAT_ID": 2,
                               "Kategorie": "Schraubendreher",
                               "STK": "45",
                               "Bezeichnung": "Kreuzschlitzdreher"
                },
                {
                               "MAT_ID": 3,
                               "Kategorie": "Schrauben",
                               "STK": "450",
                               "Bezeichnung": "Kreuzschlitzschrauben"
                },
                {
                               "MAT_ID": 4,
                               "Kategorie": "Schrauben",
                               "STK": "500",
                               "Bezeichnung": "Schlitzschrauben"
                },
                {
                               "MAT_ID": 5,
                               "Kategorie": "Nägel",
                               "STK": "700",
                               "Bezeichnung": "Zimmermannsnägel"
                },
                {
                               "MAT_ID": 6,
                               "Kategorie": "Reinigungstücher",
                               "STK": "30",
                               "Bezeichnung": "Microfaser-Lappen"
                },
                {
                               "MAT_ID": 7,
                               "Kategorie": "Befestigungsmaterial",
                               "STK": "50",
                               "Bezeichnung": "Sekundenkleber"
                }],
                kunden: [{
                               "KUN_ID": 1,
                               "Name": "KO/DU GmbH",
                               "Anschrift": "ABC-Straße 133",
                               "Ort": "Oldenburg",
                               "Branche": "Partnervermittlung"
                },
                {
                               "KUN_ID": 2,
                               "Name": "Hans & Jens GmbH",
                               "Anschrift": "Hafenstr. 89",
                               "Ort": "Bremerhaven",
                               "Branche": "Finazierungsdienstleister"
                },
                {
                               "KUN_ID": 3,
                               "Name": "Soqudo GmbH",
                               "Anschrift": "Georgstr. 217",
                               "Ort": "Bremerhaven",
                               "Branche": "Verpackungsdienstleister"
                },
                {
                               "KUN_ID": 4,
                               "Name": "Landschaftsbauer Bauers Meiers GmbH",
                               "Anschrift": "St.-Gerorg-Str. 22B",
                               "Ort": "Rotenburg-Wümme",
                               "Branche": "Landschaftsbauer"
                },
                {
                               "KUN_ID": 5,
                               "Name": "VM Haus Maier",
                               "Anschrift": "Konsult-schmidt-Str. 137",
                               "Ort": "Bremen",
                               "Branche": "Automobiel Verkauf"
                },
                {
                               "KUN_ID": 6,
                               "Name": "Elektroguter e.K.",
                               "Anschrift": "Fährstraße 55",
                               "Ort": "Hamburg",
                               "Branche": "Einzelhandelskaufmann für Elektrogüter"
                },
                {
                               "KUN_ID": 7,
                               "Name": "Güven OHG",
                               "Anschrift": "Sübenplatz 10",
                               "Ort": "Hamburg",
                               "Branche": "Gastronomie"
                },
                {
                               "KUN_ID": 8,
                               "Name": "Griechische Spezialitäten Olympus",
                               "Anschrift": "Siebenbrüderweide 56",
                               "Ort": "Hamburg",
                               "Branche": "Gastronomie"
                },
                {
                               "KUN_ID": 9,
                               "Name": "Hanse Partische Allianz GmbH",
                               "Anschrift": "Spitalerstr. 135",
                               "Ort": "Hamburg",
                               "Branche": "Finazierungsdienstleister"
                },
                {
                               "KUN_ID": 10,
                               "Name": "Kaiser & Kaiser",
                               "Anschrift": "Wihlem-Kaiser-Platz 71",
                               "Ort": "Cuxhaven",
                               "Branche": "Immobielenmakler"
                },
                {
                               "KUN_ID": 11,
                               "Name": "Ship Shore",
                               "Anschrift": "Wihlem-Kaiser-Platz 71",
                               "Ort": "Oldenburg",
                               "Branche": "Immobielenmakler"
                }
                	],
               	personal :[{
                               "Perso_Id": 1,
                               "Vorname": "Hans",
                               "Name": "Heinz",
                               "B_Day": "15.04.1986",
                               "Taetigkeit": "Elektriker"
                },
                {
                               "Perso_Id": 2,
                               "Vorname": "Karl",
                               "Name": "Heinz",
                               "B_Day": "20.07.1980",
                               "Taetigkeit": "Monteur"
                },
                {
                               "Perso_Id": 3,
                               "Vorname": "Andreas",
                               "Name": "von Guten",
                               "B_Day": "01.03.1991",
                               "Taetigkeit": "monteur"
                },
                {
                               "Perso_Id": 4,
                               "Vorname": "Emanuel",
                               "Name": "Groß",
                               "B_Day": "27.11.1992",
                               "Taetigkeit": "Elektriker"
                },
                {
                               "Perso_Id": 5,
                               "Vorname": "Hans",
                               "Name": "Unterseher",
                               "B_Day": "05.02.1979",
                               "Taetigkeit": "Fachinformatiker für Systemintegration"
                },
                {
                               "Perso_Id": 6,
                               "Vorname": "Anna",
                               "Name": "Kanns",
                               "B_Day": "30.06.1989",
                               "Taetigkeit": "Beraterin"
                },
                {
                               "Perso_Id": 7,
                               "Vorname": "Dell ",
                               "Name": "Gut",
                               "B_Day": "23.10.1980",
                               "Taetigkeit": "Elektriker"
                },
                {
                               "Perso_Id": 8,
                               "Vorname": "Sam",
                               "Name": "von Haven",
                               "B_Day": "16.07.1980",
                               "Taetigkeit": "Elektriker"
                },
                {
                               "Perso_Id": 9,
                               "Vorname": "Wolfgang",
                               "Name": "Kühle",
                               "B_Day": "28.12.1968",
                               "Taetigkeit": "Elektriker"
                },
                {
                               "Perso_Id": 10,
                               "Vorname": "Manfred",
                               "Name": "Intel",
                               "B_Day": "09.09.1979",
                               "Taetigkeit": "Elektriker"
                }
                		],
                wagen: [{
                               "Wagen_ID": 1,
                               "Marke": "VM",
                               "Modell": "Transporter T4",
                               "Hubraum": "1.798",
                               "Kilometerstand": "108.456"
                },
                               {
                               "Wagen_ID": 2,
                               "Marke": "Mercedes Benz",
                               "Modell": "Transporter Vito",
                               "Hubraum": "1.798",
                               "Kilometerstand": "108.456"
                },
                {
                               "Wagen_ID": 3,
                               "Marke": "VM",
                               "Modell": "Transporter T4",
                               "Hubraum": "1.798",
                               "Kilometerstand": "145.008"
                },
                {
                               "Wagen_ID": 4,
                               "Marke": "VM",
                               "Modell": "Transporter T4",
                               "Hubraum": "1.798",
                               "Kilometerstand": "201.456"
                },
                {
                               "Wagen_ID": 5,
                               "Marke": "Mercedes Benz",
                               "Modell": "Transporter Vito",
                               "Hubraum": "1.798",
                               "Kilometerstand": "89.967"
                },
                {
                               "Wagen_ID": 6,
                               "Marke": "Mercedes Benz",
                               "Modell": "Transporter Citan",
                               "Hubraum": "1.798",
                               "Kilometerstand": "195.891"
                }
                			
                			],
                geo: [{
                	"Lat": 20,
                	"Long": 20
                }],
                Language: "de",
                LanguageDescription: "Deutsch",
               	CurrLang: "",
               	ActivePageTitleI18nKey: "app.homePageTitle"

			});
			 //Init User Language (default in model is Deutsch, so only need to detect English)
			var userLang = navigator.language || navigator.userLanguage;
			if (userLang.indexOf("en") >= 0) {
		        oModel.setProperty("/Language", "en");
		        oModel.setProperty("/LanguageDescription", "English");
			}
		    return oModel;
		},

	};
});